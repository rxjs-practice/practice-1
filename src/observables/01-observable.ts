import { Observable, Observer } from "rxjs";

// Observer
const observer: Observer<any> = {
  next: value => console.log("siguiente [next]", value),
  error: error => console.warn("error [obs]", error),
  complete: () => console.log("completado [obs]")
};

// Observable con subscripciones
// 1. Para que un observable se ejecute debe tener una subscripción
const obs$ = new Observable<string>(subs => {
  subs.next("Hola");
  subs.next("Hey");

  subs.next("Hola");
  subs.next("Hey");

  //   Forzar un error
  //   const a = undefined
  //   a.nombre = 'Cristian'

  subs.next("Hey");

  subs.complete();

  subs.next("Hola");
  subs.next("Hey");
});

// Subscriber usando un Observer
obs$.subscribe( observer )

// Subscriber (procesa el next del subscriber)
// 1. Puede tener tres posibles agumentos:
//      a. obtener respuesta con función flecha
//      b. con un observer
//      c. definir tres callbacks o funciones
// obs$.subscribe(
//   valor => console.log("next", valor),
//   error => console.warn(error),
//   () => console.log("completado")
// );
