import { Observable, Observer, Subject } from "rxjs";

// Observer
const observer: Observer<any> = {
  next: value => console.log("next:", value),
  error: error => console.warn("error:", error),
  complete: () => console.log("completado")
};

const intervalo$ = new Observable<number>(subs => {
  const intervalId = setInterval(() => {
    subs.next(Math.random());
  }, 3000);

  return () => clearInterval(intervalId);
});

/**
 * Tiene varias caracteristicas importantes
 * 1. Casteo multiple
 * 2. Tambien es un observer
 * 3. Next, error y complete
 */
const subject$ = new Subject();
intervalo$.subscribe( subject$ )

const subs1 = subject$.subscribe(rnd => console.log('sub1', rnd));
const subs2 = subject$.subscribe(rnd => console.log('sub2', rnd));
